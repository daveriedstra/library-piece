# Library Piece

Web page for the library piece. Audio repos are elsewhere.

## To Do

* [x] test interrupted media file loading and other events
    * [x] and prevent play action when not loaded
    * [x] ended event can fire if media element runs out of local data and can't load any more. Does it continue to try loading? Do we need a special case to handle this?
        * handling onPlaying should at least cover our gui state.
* [ ] iOS safari seems kaputt?
    * use https://github.com/RemoteDebug/remotedebug-ios-webkit-adapter
    * [x] audio doesn't load
    * [x] spinner doesn't spin
    * [x] no sound when playing
    * [ ] no bg spectrogram
        * analyserNode data incorrect -- all 0s
        * critical bug - MediaElementAudioSourceNode doesn't play audio through webaudio on iOS, so this also affects the volume controls
        * fixed with AudioBuffer
* [ ] **playback clicks & stalls** on FF mobile
    * maybe also fixed with AudioBuffer approach
* [x] use ResizeObserver for canvas size

* [ ] use AudioBufferSourceNode, AudioBuffer, and decodeAudioData()
    * https://developer.mozilla.org/en-US/docs/Web/API/BaseAudioContext/decodeAudioData
    * https://developer.mozilla.org/en-US/docs/Web/API/AudioBufferSourceNode

* [ ] redesign
  * [x] responsive views
  * [ ] use user preference for light / dark mode (`prefers-color-scheme` media query)

* [ ] option to use compressed audio
* [ ] option to use downloaded audio

* [x] copy updates

* [x] play state should be set by audio element **events**, not click toggle
  * [x] ~~play pending indicator~~
  * [x] loader indicator

* [x] **load hangs** for some reason, prevents ixn sometimes
  * load state events / indicators would help debug this
  * might just be simplehttpserver?
      * ... yep.

* [x] **waveform** on hover or volume open?
  * [x] works fine
  * [x] is it clear that it's intentionally fading out and not fading out because the sound has stopped? (maybe a loading/loaded indicator would clarify?)

* [ ] **volume slider & transitions**
  * [x] finer audio control ~~(remove clicks on play / pause)~~
  * [ ] better but clicks still not fully gone in pulses audio slider
      * might be my machine?
  * [x] framerate improved with debouncing & longer timeouts, but still some clicks...
  * [x] volume slider janky
    * rampVolume possibly responsible
  * [ ] visual delay after dragging slider on iOS
  * [ ] tapping volume icon shouldn't change volume (unless slider already open)

* [x] **columns holding widgets should always be equal size**
* [x] icons should be **absolutely positioned** so replacing them doesn't reflow entire layout and cause jank

* [ ] better design
* [ ] new title?
* [x] better touch support
* [ ] UX fine-tuning
* [ ] performance audit
    * [ ] optimize canvas maybe
    * [ ] audio memory leak after refreshes
* [ ] device & browser testing
* [ ] fine-tune no-js layout
* [ ] accessibility features
* [x] ~~inline & base64 favicon (empty icon added)~~
* [ ] other optimizations from the [KISS entry](https://k1ss.org//blog/20191004a)
* [ ] build should maybe minify? at least leave out js comments...

## Building

```
make
```

This is a single page site with as few requests as possible. The build script, for now, just inlines files where mentioned with `{{path/file.ext}}` syntax. Build requires `make` and gnu `awk`.

For an auto reload build use `entr`:

```
ls src/*.* | entr make
```
