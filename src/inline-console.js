(function() {
  if (window.DEBUG_INLINE) {
    const consoleEl = document.createElement('div')
    consoleEl.style = `
      border-top: thin solid white;
      margin-top: 1rem;
      width: 100%;
    `
    document.body.appendChild(consoleEl)

    const makeArgsEl = function() {
      let logStr = ''
      for (let arg of arguments) {
        logStr += `${arg.toString()} `
      }
      const logLnEl = document.createElement('pre')
      logLnEl.innerText = logStr
      return logLnEl
    }

    window.console.log = function() {
      const logLnEl = makeArgsEl(...arguments)
      consoleEl.appendChild(logLnEl)
    }

    window.console.warn = function() {
      const logLnEl = makeArgsEl(...arguments)
      logLnEl.style = `
        color: var(--gbox-yellow-dark);
      `
      consoleEl.appendChild(logLnEl)
    }

    window.console.error = function() {
      const logLnEl = makeArgsEl(...arguments)
      logLnEl.style = `
        color: var(--gbox-red-dark);
      `
      consoleEl.appendChild(logLnEl)
    }
  }
})();
